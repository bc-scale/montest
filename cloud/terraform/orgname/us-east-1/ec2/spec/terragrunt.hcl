terraform {
  source = "../../../../modules//ec2"
}

include {
  path = find_in_parent_folders()
}

locals { 
  common_vars = yamldecode(file(find_in_parent_folders("overrides.yaml")))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"), { locals = {} })
  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl"), { locals = {} })
  aws_region = "${local.region_vars.inputs.aws_region}"
  ec2_id = "test0"
  name = "${local.common_vars.account_name}-${local.ec2_id}"

  user_data = <<-EOT
  #!/bin/bash
  echo "Hello Terraform!"
  EOT

  tags = {
     Org = "${local.common_vars.org}"
     Repo = "${local.common_vars.repo}"
     AccountID = "${local.common_vars.account_name}"
     LocalName = "${local.name}"
     Terraform = "true"
     EnvironmentType = "${local.common_vars.account_type}"
  }
}

inputs = {
  name = local.name

  ami                         = "ami-0b0dcb5067f052a63"
  instance_type               = "t3.medium"
  availability_zone           = "${local.aws_region}b"
  subnet_id                   = "subnet-0813dc466467fecd0"
  vpc_security_group_ids      = ["sg-0c1bae80ada292ed8"]
  
  associate_public_ip_address = true
  disable_api_stop            = false

  create_iam_instance_profile = true
  iam_role_description        = "IAM role for EC2 ${local.name}"
  iam_role_policies = {
    AdministratorAccess = "arn:aws:iam::aws:policy/AdministratorAccess"
  }

  # only one of these can be enabled at a time
  hibernation = true
  # enclave_options_enabled = true

  user_data_base64            = base64encode(local.user_data)
  user_data_replace_on_change = true

  enable_volume_tags = false
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 50
      tags = {
        Name = "root-block-${local.name}"
      }
    },
  ]

  ebs_block_device = [
    {
      device_name = "/dev/sdf"
      volume_type = "gp3"
      volume_size = 5
      throughput  = 200
      encrypted   = true
    }
  ]

  tags = local.tags
}
