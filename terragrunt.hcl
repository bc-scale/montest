locals {
  terraform_state_s3_bucket = "montest-tfstate"
  project_name              = "montest"
  aws_region                = "us-east-1"
  dynamodb_table            = "montest-tfstate-lock"
  overrides_path            = find_in_parent_folders("overrides.yaml")
  overrides                 = yamldecode(fileexists(local.overrides_path) ? file(local.overrides_path) : "{}")
  overridden                = fileexists(local.overrides_path) ? merge(local.overrides.s3) : {}
}

# When using this terragrunt config, terragrunt will generate the file "provider.tf" with the aws provider block before
# calling to terraform. Note that this will overwrite the `provider.tf` file if it already exists.
generate "provider" {
  path      = "provider.tf"
  if_exists = "skip" # Allow modules to override provider settings
  contents  = <<EOF
provider "aws" {
  # The AWS region in which all resources will be created
  region = "${local.aws_region}"
}
EOF
}

# Configure Terragrunt to automatically store tfstate files in an S3 bucket
remote_state {
  backend = "s3"
  config = {
    encrypt        = true
    bucket         = fileexists(local.overrides_path) ? lower(format("%s", join("", values(local.overridden)))) : local.terraform_state_s3_bucket
    key            = format("%s/%s/terraform.tfstate", local.project_name, path_relative_to_include())
    dynamodb_table = local.dynamodb_table
    region         = local.aws_region
  }

  generate = {
    path      = "remote.tf"
    if_exists = "overwrite_terragrunt"
  }
}

terraform {

  before_hook "before_hook" {
    commands = ["init", "apply", "plan"]
    execute  = ["echo", "Running Terraform"]
  }

  extra_arguments "retry_lock" {
    commands = get_terraform_commands_that_need_locking()

    arguments = [
      "-lock-timeout=20m"
    ]
  }

}

