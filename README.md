# montest
Stage environment configuration and base components deployment:
- k3d
- helm
- terraform/terragrunt
- prometheus
- node-exporter (linux)
- metrics-server
- grafana
- gitlab-agent

```bash
.
├── README.md                    # Please, update README.md accordingly 
├── cloud                        # Cloud specs
│   ├── k8s
│   │   └── helm                 # helm charts and environments
│   │       ├── components
│   │       │   ├── echo-server
│   │       │   ├── grafana
│   │       │   └── prometheus
│   │       └── environments     # values.ayml for multienvironment's deployments
│   │           └── mc
│   └── terraform                # IaC with terraform
│       ├── modules              # local modules
│       │   └── ec2
│       │       ├── main.tf
│       │       ├── outputs.tf
│       │       ├── variables.tf
│       │       └── versions.tf
│       └── orgname             # AWS org and multienvs specs
│           ├── account.hcl
│           └── us-east-1
│               ├── ec2         # install ec2
│               ├── overrides.yaml
│               └── region.hcl
└── terragrunt.hcl
```

## Staging environment 
### Terraform and terragrunt
```bash
# install teraform and terragrunt
## Install
brew install terraform
brew install terragrunt

## export AWS vars
export AWS_DEFAULT_REGION=us-east-1
export AWS_SECRET_ACCESS_KEY="KEY"
export AWS_ACCESS_KEY_ID="ID"

## provisioning 'test0' for AWS account_id 'orgname'
terragrunt run-all plan --terragrunt-working-dir cloud/terraform/orgname/us-east-1/ec2/spec
terragrunt run-all apply --terragrunt-working-dir cloud/terraform/orgname/us-east-1/ec2/spec
```
### SSH to a host
```bash
# request 'sshkey' from the devops group
ssh ubuntu@44.224.108.1 -i .ssh/sre-tmp-key.pem
```

### Install K3d and default components
```bash
# install docker engine
apt-get install     ca-certificates     curl     gnupg     lsb-release
mkdir -p /etc/apt/keyrings
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# install k3d on top of docker
curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo service docker start
sudo k3d cluster create mc
sudo k3d kubeconfig get mc
kubectl cluster-info

# install helm
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

# install gitlab-agent
   helm repo add gitlab https://charts.gitlab.io
   helm repo update
   helm upgrade --install k3d gitlab/gitlab-agent     --namespace gitlab-agent     --create-namespace     --set image.tag=v15.7.0-rc1     --set config.token=ENV.TOKEN     --set config.kasAddress=wss://kas.gitlab.com
# install echo-server
helm upgrade echo cloud/k8s/helm/components/echo-server -i -n default
```
### CICD with gitlab (GitOps CD)
```bash
mkdir -p .gitlab/agents/k3d
echo "---" > .gitlab/agents/k3d/config.yaml
```
Config.yaml
```yaml
gitops:
  manifest_projects:
  - id: ehllo/montest
    default_namespace: default
    paths:
      # Read all YAML files from this directory.
    - glob: '/team1/app1/*.yaml'
      # Read all .yaml files from team2/apps and all subdirectories.
    - glob: '/team2/apps/**/*.yaml'
      # If 'paths' is not specified or is an empty list, the configuration below is used.
    - glob: '/**/*.{yaml,yml,json}'
    reconcile_timeout: 3600s
    dry_run_strategy: none
    prune: true
    prune_timeout: 3600s
    prune_propagation_policy: foreground
    inventory_policy: must_match
  charts:
  - release_name: echo
    source:
      project:
        id: ehllo/montest
        path: cloud/k8s/helm/components/echo-server/charts/echo-server
    namespace: default
    max_history: 2
    values:
      - inline:
          someKey: example value
```
## K8s components
### Prometheus
#### Install Prometheus
```bash
helm dependency build cloud/k8s/helm/components/prometheus
helm upgrade prom cloud/k8s/helm/components/prometheus --install
```
#### Connect to Prometheus
```bash
The Prometheus server can be accessed via port 80 on the following DNS name from within your cluster:
prom-prometheus-server.default.svc.cluster.local


Get the Prometheus server URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace default port-forward $POD_NAME 9090


The Prometheus alertmanager can be accessed via port  on the following DNS name from within your cluster:
prom-prometheus-%!s(<nil>).default.svc.cluster.local


Get the Alertmanager URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace default port-forward $POD_NAME 9093

```

### Grafana
#### Install Grafana
```bash
helm upgrade --install grafana cloud/k8s/helm/components/grafana
```
#### Connect to Grafana
```bash
1. Get your 'admin' user password by running:

   kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

2. The Grafana server can be accessed via port 80 on the following DNS name from within your cluster:

   grafana.default.svc.cluster.local

   Get the Grafana URL to visit by running these commands in the same shell:
     export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana" -o jsonpath="{.items[0].metadata.name}")
     kubectl --namespace default port-forward $POD_NAME 3000

3. Login with the password from step 1 and the username: admin
```
